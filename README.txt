Webform i18n Extras
===================

Motivation

When a site required multilingual webforms, installing webform_localization (http://www.drupal.org/project/webform_localization) provides the ability to translate strings via the backend translation interface, which works really well. However, the workflow for creating a translated webform is challenging because the user needs to do the following:

1. Create the webform in the primary site language

2. Go to the backend interface and search for ALL the strings for questions / options and enter translations.

This process is tedious and requires content editors to navigate to the backend translation interface.

This module changes the workflow by adding translate links beside each webform component which link directly to the string entry in the backend interface. It also looks for "component groups" and adds the component option strings to the component string translation form, which enables the content editor to translate a question and the options at the same time.

The module also provides a link to translate the form confirmation message.

References

http://www.drupal.org/node/2382607 - Initial discussion on wetkit project
http://www.drupal.org/node/2401549 - Issue to add this code to webform_localization

Installation

1. Enable module as per normal

2. Enable "String translation" on a webform
